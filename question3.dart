class MyApps {
  List<String> Category = [
    "Public",
    "Private",
    "Private",
    "Public",
    "Public",
    "Public",
    "Private",
    "Private",
    "Public",
    "Private"
  ];

  List<String> Developer = [
    "Frederick Kobo",
    "Kobus Ehlers",
    "Jasper Van Heesch",
    "Simon Hartley",
    "Thato Marumo",
    "Arno Von Helden",
    "Matthew Piper",
    "Alex Thomson",
    "Charles Savage",
    "Makundi Lambani"
  ];

  List<String> Year = [
    "2012",
    "2013",
    "2014",
    "2015",
    "2016",
    "2017",
    "2018",
    "2019",
    "2020",
    "2021"
  ];

  List CapsLock(List MyApps) {
    List<String> CapsLockList = [];
    for (var l = 0; l < MyApps.length; l++) {
      CapsLockList.add(MyApps[l].toString().toUpperCase());
    }
    return CapsLockList;
  }
}

void main(List<String> args) {
  List<String> AppList = [
    "FNB Banking",
    "SnapScan",
    "Live Inspect",
    "WumDrop",
    "Domestly",
    "Standard Bank Shyft",
    "Khula",
    "Naked Insurance",
    "Easy Equities",
    "Ambani"
  ];

  MyApps ObjectApp = new MyApps();
  List<dynamic> AppBrand = ObjectApp.CapsLock(AppList);
  List<dynamic> Category = ObjectApp.Category;
  List<dynamic> AppDevs = ObjectApp.Developer;
  List<dynamic> AppYear = ObjectApp.Year;

  for (var e = 0; e < ObjectApp.CapsLock(AppList).length; e++) {
    var AppLabel = AppBrand[e];
    var Sector = Category[e];
    var Developer = AppDevs[e];
    var Year = AppYear[e];

    print(
        "[AppBrand : $AppLabel || Sector : $Sector || AppDevs : $Developer || AppYear : $Year]");
  }
}
